<?php

namespace JVG\MandrillBundle;

use JVG\MandrillBundle\DependencyInjection\Compiler\MessageEntityConfigurationPass;
use Oro\Bundle\EntityBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\ClassLoader\UniversalClassLoader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use JVG\MandrillBundle\DependencyInjection\Compiler\SecurityLoaderPass;
use JVG\MandrillBundle\DependencyInjection\Compiler\DoctrineFunctionLoaderPass;
use Symfony\Component\HttpKernel\KernelInterface;

class JVGMandrillBundle extends Bundle
{
    const ENTITY_PROXY_NAMESPACE   = 'MessageEntity';
    const CACHED_ENTITIES_DIR_NAME = 'jvg_mandrill';


    /**
     * Constructor
     *
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $loader = new UniversalClassLoader();
        $loader->registerNamespaces(
            [
                self::ENTITY_PROXY_NAMESPACE =>
                    $kernel->getCacheDir() . DIRECTORY_SEPARATOR . self::CACHED_ENTITIES_DIR_NAME
            ]
        );
        $loader->register();
    }

    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new DoctrineFunctionLoaderPass());
        $container->addCompilerPass(new SecurityLoaderPass());
        $container->addCompilerPass(new MessageEntityConfigurationPass());
        $this->addDoctrineOrmMappingsPass($container);
    }

    /**
     * Add a compiler pass handles ORM mappings of message proxy
     *
     * @param ContainerBuilder $container
     */
    protected function addDoctrineOrmMappingsPass(ContainerBuilder $container)
    {
        $entityCacheDir = sprintf(
            '%s%s%s%s%s',
            $container->getParameter('kernel.cache_dir'),
            DIRECTORY_SEPARATOR,
            self::CACHED_ENTITIES_DIR_NAME,
            DIRECTORY_SEPARATOR,
            str_replace('\\', DIRECTORY_SEPARATOR, self::ENTITY_PROXY_NAMESPACE)
        );

        $container->setParameter('jvg_mandrill.message_entity.cache_dir', $entityCacheDir);
        $container->setParameter('jvg_mandrill.message_entity.cache_namespace', self::ENTITY_PROXY_NAMESPACE);
        $container->setParameter('jvg_mandrill.message_entity.proxy_name_template', '%sProxy');

        // Ensure the cache directory exists
        $fs = new Filesystem();
        if (!is_dir($entityCacheDir)) {
            $fs->mkdir($entityCacheDir, 0777);
        }

        $container->addCompilerPass(
            DoctrineOrmMappingsPass::createYamlMappingDriver(
                [$entityCacheDir => self::ENTITY_PROXY_NAMESPACE]
            )
        );
    }
}
