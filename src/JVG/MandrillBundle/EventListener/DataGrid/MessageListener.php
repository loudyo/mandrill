<?php

namespace JVG\MandrillBundle\EventListener\Datagrid;

use JVG\MandrillBundle\Datagrid\MessageEntityQueryFactory;
use Oro\Bundle\DataGridBundle\Datasource\Orm\OrmDatasource;
use Oro\Bundle\DataGridBundle\Event\BuildAfter;


class MessageListener
{
    /**
     * @var MessageEntityQueryFactory
     */
    protected $factory;

    /**
     * @param MessageEntityQueryFactory $factory
     */
    public function __construct(MessageEntityQueryFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Add required filters
     *
     * @param BuildAfter $event
     */
    public function onBuildAfter(BuildAfter $event)
    {
        /** @var OrmDatasource $ormDataSource */
        $ormDataSource = $event->getDatagrid()->getDatasource();
        $queryBuilder = $ormDataSource->getQueryBuilder();
        $parameters = $event->getDatagrid()->getParameters();

        $this->factory->prepareQuery($queryBuilder);

        if (array_key_exists('messageIds', $parameters)) {
            $emailIds = $parameters['messageIds'];
            if (!is_array($emailIds)) {
                $emailIds = explode(',', $emailIds);
            }
            $queryBuilder->andWhere($queryBuilder->expr()->in('m.id', $emailIds));
        }
    }
}
