<?php

namespace JVG\MandrillBundle\EventListener;

use Oro\Bundle\TagBundle\Entity\Tag;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use JVG\MandrillBundle\Entity\Message;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MessageSubscriber implements EventSubscriber
{

    protected $manager;

    /**
     * @var Tag[]
     */
    protected $tags;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function getSubscribedEvents()
    {
        return array('postPersist');
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        if (($resource = $args->getEntity()) and $resource instanceof Message) {
            if (is_null($this->manager) && $this->container) {
                $this->manager = $this->container->get('oro_tag.tag.manager');
            }
            if (is_null($this->tags)
                && $this->container->get('jvg_mandrill.form.subscriber.tags')
                && $tags = $this->container->get('jvg_mandrill.form.subscriber.tags')->getTags()
            ) {
                $this->tags = $tags;
            }
            if ($this->tags) {
                $resource->setTags($this->tags);
                $this->manager->saveTagging($resource);
            }
        }
    }
}
