/*global define*/
define(['jquery','underscore', 'orotranslation/js/translator', 'oroui/js/modal', 'mandrill', 'orolocale/js/formatter/datetime','orodatagrid/js/datagrid/action/abstract-action'
    ], function ($, _, __, Modal, mandrill, dateTimeFormatter, AbstractAction) {
    'use strict';

    return AbstractAction.extend({

        /** @property {Object} */
        modal: null,
        
        /** @property {Object} */
        template: _.template($('#template-message-result').html()),
        title: "Message details",

        getDialog: function (data) {
            if (!this.modal) {
                this.modal = (new Modal({
                    title: this.title,
                    content: this.template(data),
                    okText: "Ok"
                }));
            }
            return this.modal;
        },

        showMessage: function (message) {
            message.datetime = dateTimeFormatter.convertDateTimeToBackendFormat(new Date(message.ts*1000));
            this.getDialog(message).open();
        },

        showError: function (message) {
            this.template = _.template('<div class="row-fluid form-horizontal"><div class="responsive-block">' +
                '<% if (code) { %><div class="control-group"><label class="control-label">Code</label><div class="controls"><div class="control-label"><%= code %></div></div></div><% } %>' +
                '<% if (message) { %><div class="control-group"><label class="control-label">Message</label><div class="controls"><div class="control-label"><%= message %></div></div></div><% } %>' +
                '</div></div>');
            this.title = message.name;
            this.getDialog(message).open();
        },

        /**
         * Execute delete model
         */
        execute: function () {
            var message = mandrill.messages.info({'id': this.model.get('mandrillId')}, _.bind(this.showMessage, this), _.bind(this.showError, this));
        }
    });
});
