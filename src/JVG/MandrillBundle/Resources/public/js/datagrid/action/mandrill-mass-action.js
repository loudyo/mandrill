/*global define*/
define(['underscore', 'orodatagrid/js/datagrid/action/mass-action', 'oroui/js/messenger'],
    /**
     * @param {underscore} _
     * @param {MassAction} MassAction
     * @param {notificationFlashMessage} messenger
     * @returns {*|Object|void}
     */
    function (_, MassAction, messenger) {
        'use strict';

        /**
         * Merge mass action class.
         *
         * @export  jvgmandrill/js/datagrid/action/merge-mass-action
         * @class   jvgmandrill.datagrid.action.MandrillMassAction
         * @extends orodatagrid.datagrid.action.MassAction
         */
        return MassAction.extend({
            /** @property {Object} */
            defaultMessages: {
                confirm_title: 'Mandrill Confirmation',
                confirm_content: 'Are you sure you want to mail selected items?',
                confirm_ok: 'Yes, Mandrill',
                success: 'Selected items were mailed.',
                error: 'Selected items were not mailed.',
                empty_selection: 'Please, select items to mail.'
            }
        });

    });
