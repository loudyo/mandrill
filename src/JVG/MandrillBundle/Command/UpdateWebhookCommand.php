<?php

namespace JVG\MandrillBundle\Command;

use Oro\Bundle\CronBundle\Command\CronCommandInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateWebhookCommand extends ContainerAwareCommand implements CronCommandInterface
{
    /**
     * {@inheritDoc}
     */
    public function  getDefaultDefinition()
    {
        return '* * * * 7'; // every week
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('oro:cron:webhook:update')
            ->setDescription('Update webhook key.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $key = $container->get('jvg_mandrill.mailer')->updateWebHookKey();

        $config = $container->get('oro_config.global');
        $config->set('jvg_mandrill.webhook_key', $key);
        $config->flush();
    }

}
