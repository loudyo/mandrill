<?php
namespace JVG\MandrillBundle;
/**
 * Sends Messages over SMTP with ESMTP support.
 */
class MandrillTransport extends \Swift_SmtpTransport
{
    /**
     * Create a new SmtpTransport, optionally with $host, $port and $security.
     *
     * @param string  $host
     * @param integer $port
     * @param string  $security
     */
    public function __construct($host = 'localhost', $port = 25, $security = null)
    {
        call_user_func_array(
            array($this, 'Swift_Transport_EsmtpTransport::__construct'),
            \Swift_DependencyContainer::getInstance()
                ->createDependenciesFor('transport.smtp')
            );

        $this->setHost($host);
        $this->setPort($port);
        $this->setEncryption($security);
    }

    /**
     * Create a new SmtpTransport instance.
     *
     * @param string  $host
     * @param integer $port
     * @param string  $security
     *
     * @return MandrillTransport
     */
    public static function newInstance($host = 'localhost', $port = 25, $security = null)
    {
        return new self($host, $port, $security);
    }
}
