<?php

namespace JVG\MandrillBundle\Form\Handler;

use JVG\MandrillBundle\Entity\Manager\MessageEntityManager;
use JVG\MandrillBundle\Entity\Message;
use JVG\MandrillBundle\Mailer;
use JVG\MandrillBundle\Model\MailableEntity;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class EntityHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @var MailableEntity
     */
    protected $mailableEntity;

    /**
     * @var String
     */
    protected $entityFormField;

    /**
     * @var MessageEntityManager
     */
    protected $messageEntityManager;

    /**
     * @param FormInterface  $form
     * @param Request        $request
     * @param EntityManager  $em
     * @param Mailer         $mailer
     * @param MailableEntity $mailableEntity
     * @param String         $entityFormField
     * @param MessageEntityManager $messageEntityManager
     */
    public function __construct(FormInterface $form, Request $request, EntityManager $em, Mailer $mailer, MailableEntity $mailableEntity, MessageEntityManager $messageEntityManager, $entityFormField)
    {
        $this->form            = $form;
        $this->request         = $request;
        $this->em              = $em;
        $this->mailer          = $mailer;
        $this->mailableEntity  = $mailableEntity;
        $this->entityFormField = $entityFormField;
        $this->messageEntityManager  = $messageEntityManager;
    }

    /**
     * Process form
     * @return bool True on successful processing, false otherwise
     */
    public function process()
    {
        if (in_array($this->request->getMethod(), array('POST', 'PUT'))) {

            $this->form->handleRequest($this->request);

            if ($this->form->isValid()) {
                $data = $this->form->getData();

                if ($scheduleDateTime = $data['date']) {
                    $this->mailer->setSendAt($scheduleDateTime);
                }
                if ($tags = $data['tags']['all']) {
                    $this->mailer->setTags($tags);
                }
                $this->mailer->setTemplate($data['template']);
                $entities = $data[$this->entityFormField];

                $mailables = array();
                foreach($entities as $item) {
                    $mailable = $this->mailableEntity->factory($item);

                    if(!$mailable->isMailable()){
                        continue;
                    }
                    $this->mailer->addEntity($mailable);
                    $mailables[] = $mailable;
                }

                $results = $this->mailer->send();

                array_map(function($result, $mailable) {
                    $message = new Message($result);
                    $messageEntity = $this->messageEntityManager->newMessageEntity()->setEntity($mailable);
                    $message->setMessageEntity($messageEntity);
                    if (isset($scheduleDateTime)) $message->setScheduledAt($scheduleDateTime);
                    $this->em->persist($message);
                }, $results, $mailables);
                $this->em->flush();
                return true;
            }
        }
        return false;
    }
}
