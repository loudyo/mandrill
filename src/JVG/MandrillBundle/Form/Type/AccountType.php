<?php

namespace JVG\MandrillBundle\Form\Type;

use JVG\MandrillBundle\Form\EventSubscriber\TagSubscriber;
use Oro\Bundle\FormBundle\Config\FormConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AccountType extends AbstractType
{

    /**
     * @var TagSubscriber
     */
    protected $subscriber;

    public function __construct(TagSubscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber($this->subscriber);
        $builder->add(
            'template',
            'jvg_mandrill_template_choice',
            array(
                'label'    => 'Template',
                'required' => true
            )
        );
        $builder->add(
            'tags',
            'oro_tag_select',
            array(
                'label' => 'Tags',
                'data' => array('all' => array())
            )
        );
        $builder->add(
            'date',
            'oro_datetime',
            array(
                'label'    => 'At'
            )
        );

        $builder->add(
            'accounts',
            'jvg_mandrill_collection',
            array(
                'type'                  => 'jvg_mandrill_account_select',
                'required'              => true,
            )
        );

    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'intention'            => 'entity',
                'cascade_validation'   => true,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'jvg_mandrill_entity';
    }
}
