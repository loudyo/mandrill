<?php

namespace JVG\MandrillBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;

use Oro\Bundle\FormBundle\Form\Type\ChoiceListItem;

class TemplateChoiceType extends AbstractType
{
    const NAME = 'jvg_mandrill_template_choice';

    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * Constructor
     *
     * @param ObjectManager $provider
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $choices = $this->getChoices();

        $defaultConfigs = array(
            'placeholder'             => 'Select template',
            'result_template_twig'    => 'JVGMandrillBundle:Mandrill:template/result.html.twig',
            'selection_template_twig' => 'JVGMandrillBundle:Mandrill:template/selection.html.twig',
        );
        $that = $this;
        // this normalizer allows to add/override config options outside.
        $configsNormalizer = function (Options $options, $configs) use (&$defaultConfigs, $that) {
            return array_merge($defaultConfigs, $configs);
        };

        $resolver->setDefaults(
            array(
                'choices'     => $choices,
                'empty_value' => '',
                'configs'     => $defaultConfigs
            )
        );
        $resolver->setNormalizers(
            array(
                'configs' => $configsNormalizer
            )
        );
    }

    /**
     * Returns a list of choices
     *
     * @return array of templates
     *               key = template name, value = ChoiceListItem
     */
    protected function getChoices()
    {
        $choices = [];

        $templates = $this->manager->getRepository('OroEmailBundle:EmailTemplate')->findBy(array('entityName' => 'OroCRM\Bundle\AccountBundle\Entity\Account'));
        foreach ($templates as $template) {
            $attributes = [];
            $attributes['data-label'] = $template->getName(); // data-[attribute]
            $attributes['data-icon'] = null;
            $choices[$template->getId()] = new ChoiceListItem(
                $template->getName(),
                $attributes
            );
        }

        return $choices;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'genemu_jqueryselect2_choice';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::NAME;
    }
}
