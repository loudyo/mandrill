<?php

namespace JVG\MandrillBundle\Form\Extension;


use JVG\MandrillBundle\Form\DataTransformer\EmailTemplateTransformer;
use JVG\MandrillBundle\Form\EventSubscriber\EmailTemplateSubscriber;
use JVG\MandrillBundle\Mailer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmailTemplateTypeExtension extends AbstractTypeExtension
{
    /**
     * EmailTemplateSubscriber $eventSubscriber
     */
    private $eventSubscriber;

    /**
     * Constructor.
     *
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om, Mailer $mailer)
    {
        $this->eventSubscriber = new EmailTemplateSubscriber($om, $mailer);
    }

    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return 'oro_email_emailtemplate';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber($this->eventSubscriber);
        $builder->add(
            'sync',
            'choice',
            array(
                'label'    => 'Sync',
                'mapped'   => false,
                'choices'  => array(
                    '0' => 'No',
                    '1'  => 'Yes'
                ),
                'required' => true
            )
        );
    }
}