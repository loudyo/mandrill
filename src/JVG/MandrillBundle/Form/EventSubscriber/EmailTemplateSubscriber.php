<?php
namespace JVG\MandrillBundle\Form\EventSubscriber;

use JVG\MandrillBundle\Entity\EmailTemplateSync;
use JVG\MandrillBundle\Mailer;
use JVG\MandrillBundle\Model\EmailTemplateUtils;
use Oro\Bundle\EmailBundle\Entity\EmailTemplate;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormView;

class EmailTemplateSubscriber implements EventSubscriberInterface
{
    /**
    * ObjectManager $om
    */
    private $om;

    /**
     * Bool $sync
     */
    private $sync;

    /**
     * Mailer $mailer
     */
    private $mailer;

    /**
     * Constructor.
     *
     * @param ObjectManager $om
     * @param Mailer $mailer
     */
    public function __construct(ObjectManager $om, Mailer $mailer)
    {
        $this->om = $om;
        $this->mailer = $mailer;
        $this->sync = false;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::POST_SUBMIT   => 'postSubmit',
            FormEvents::PRE_SUBMIT   => 'preSubmit'
        );
    }
    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        if ($data && $templateSync = $this->getEmailTemplateSync($data)) {
            $form->add(
                'sync',
                'choice',
                array(
                    'label'    => 'Sync',
                    'mapped'   => false,
                    'choices'  => array(
                        '0' => 'No',
                        '1'  => 'Yes'
                    ),
                    'required' => true,
                    'data' => $templateSync->getSync()
                )
            );
        }
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $this->sync = $data['sync'];
    }

    /**
     * @param EmailTemplate $data
     * @return EmailTemplateSync
     */
    protected function getEmailTemplateSync(EmailTemplate $data) {
        return $this->om->getRepository('JVGMandrillBundle:EmailTemplateSync')->findOneBy(array('emailTemplate' => $data));
    }

    /**
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        if (!$data = $event->getData()) return;
        if (!$templateSync = $this->getEmailTemplateSync($data)) {
            $templateSync = new EmailTemplateSync();
            $templateSync->setEmailTemplate($data);
        }
        $templateSync->setSync($this->sync);
        $this->setMandrillTemplate($data);
        $this->om->persist($templateSync);
        $this->om->flush();
    }

    /**
     * @param EmailTemplate $template
     */
    protected function setMandrillTemplate(EmailTemplate $template)
    {
        try {
            $this->mailer->templates->delete($template->getName());
        } catch (\Mandrill_Unknown_Template $e) {
        }
        if($this->sync)
            $this->mailer->templates->add($template->getName(), null, null, null, EmailTemplateUtils::getCode($template->getContent()));
    }
}
