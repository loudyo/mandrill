<?php

namespace JVG\MandrillBundle\Form\EventSubscriber;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Oro\Bundle\TagBundle\Entity\Taggable;
use Oro\Bundle\TagBundle\Entity\TagManager;
use Oro\Bundle\TagBundle\Entity\Tag;

class TagSubscriber implements EventSubscriberInterface
{
    /**
     * @var TagManager
     */
    protected $manager;

    /**
     * @var Tag[]
     */
    protected $tags;


    public function __construct(TagManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SUBMIT    => 'postSubmit'
        );
    }

    /**
     * Transform submitted data to model data
     *
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $values = $event->getData();
        if (isset($values['tags']['all'][0]))
            $this->tags = $values['tags'];
    }

    /**
     * Tags getter
     *
     * @return Tag[] $tags
     */
   public function getTags(){
       return $this->tags;
   }
}
