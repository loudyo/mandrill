<?php

namespace JVG\MandrillBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * EmailTemplateSync
 *
 * @ORM\Table(name="jvg_mandrill_emailtemplatesync")
 * @ORM\Entity
 *
 */
class EmailTemplateSync
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $sync;

    /**
     * @ORM\OneToOne(targetEntity="Oro\Bundle\EmailBundle\Entity\EmailTemplate")
     * @ORM\JoinColumn(name="emailtemplate_id", referencedColumnName="id")
     */
    protected $emailTemplate;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sync
     *
     * @return EmailTemplateSync
     */
    public function setSync($sync)
    {
        $this->sync = $sync;

        return $this;
    }

    /**
     * Get sync
     *
     * @return int
     */
    public function isSync()
    {
        return $this->sync;
    }

    /**
     * Set account
     *
     * @param \Oro\Bundle\EmailBundle\Entity\EmailTemplate $emailTemplate
     * @return EmailTemplateSync
     */
    public function setEmailTemplate(\Oro\Bundle\EmailBundle\Entity\EmailTemplate $emailTemplate = null)
    {
        $this->emailTemplate = $emailTemplate;

        return $this;
    }

    /**
     * Get account
     *
     * @return \OroCRM\Bundle\AccountBundle\Entity\Account 
     */
    public function getEmailTemplate()
    {
        return $this->emailTemplate;
    }

    /**
     * Get sync
     *
     * @return boolean 
     */
    public function getSync()
    {
        return $this->sync;
    }
}
