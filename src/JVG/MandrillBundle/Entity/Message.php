<?php

namespace JVG\MandrillBundle\Entity;

use JVG\MandrillBundle\Model\MailableInterface;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Oro\Bundle\TagBundle\Entity\Taggable;
/**
 * Message
 *
 * @ORM\HasLifecycleCallbacks
 *
 * Message
 * @ORM\Entity
 * @ORM\Table(name="jvg_mandrill_message")
 */

class Message implements Taggable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mandrill_id", type="string", length=40)
     */
    private $mandrillId;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="reject_reason", type="string", length=255, nullable=true)
     */
    private $rejectReason;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @var MessageEntity
     *
     * @ORM\ManyToOne(targetEntity="MessageEntity", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="message_entity_id", referencedColumnName="id", nullable=false)
     */
    protected $messageEntity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $scheduledAt;

    /**
     * @var ArrayCollection $tags
     */
    protected $tags;

    /*
    * @param array         $result
    */
    public function __construct(array $result = null)
    {
        if ($result) {
            $this->mandrillId     = $result['_id'];
            $this->email          = $result['email'];
            $this->status         = $result['status'];
            $this->rejectReason   = $result['reject_reason'];
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Message
     */
    protected function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set mandrillId
     *
     * @param string $mandrillId
     * @return Message
     */
    public function setMandrillId($mandrillId)
    {
        $this->mandrillId = $mandrillId;

        return $this;
    }

    /**
     * Get mandrillId
     *
     * @return string 
     */
    public function getMandrillId()
    {
        return $this->mandrillId;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Message
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set rejectReason
     *
     * @param string $rejectReason
     * @return Message
     */
    public function setRejectReason($rejectReason)
    {
        $this->rejectReason = $rejectReason;

        return $this;
    }

    /**
     * Get rejectReason
     *
     * @return string 
     */
    public function getRejectReason()
    {
        return $this->rejectReason;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Message
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get entity
     *
     * @return MessageEntity
     */
    public function getMessageEntity() {
        return $this->messageEntity;
    }

    /**
     * Set message entity
     *
     * @param MessageEntity $messageEntity
     * @return Message
     */
    public function setMessageEntity(MessageEntity $messageEntity){
        $this->messageEntity = $messageEntity;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Message
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set scheduledAt
     *
     * @param \DateTime $scheduledAt
     * @return Message
     */
    public function setScheduledAt($scheduledAt)
    {
        $this->scheduledAt = $scheduledAt;

        return $this;
    }

    /**
     * Get scheduledAt
     *
     * @return \DateTime
     */
    public function getScheduledAt()
    {
        return $this->scheduledAt;
    }
    /**
     * Pre persist event listener
     *
     * @ORM\PrePersist
     */
    public function beforeSave()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
    }

    /**
     * Pre update event handler
     *
     * @ORM\PreUpdate
     */
    public function doPreUpdate()
    {
        $this->updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
    }

    /**
     * {@inheritdoc}
     */
    public function getTaggableId()
    {
        return $this->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getTags()
    {
        $this->tags = $this->tags ?: new ArrayCollection();

        return $this->tags;
    }

    /**
     * {@inheritdoc}
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }
}
