<?php

namespace JVG\MandrillBundle\Entity;

use JVG\MandrillBundle\Model\MailableInterface;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Oro\Bundle\TagBundle\Entity\Taggable;
/**
 * MessageEntity
 *
 * @ORM\HasLifecycleCallbacks
 *
 * Message
 * This class is dynamically extended based of message entity providers.
 * @ORM\MappedSuperclass
 */

abstract class MessageEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Message
     */
    protected function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get entity
     *
     * @return MailableInterface
     */
    abstract public function getEntity();

    /**
     * Set message entity
     *
     * @param MailableInterface|null $entity
     * @return Message
     */
    abstract public function setEntity(MailableInterface $entity = null);

    /**
     * Get a human-readable representation of this object.
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf('MessageEntity(%s)', $this->id);
    }
}
