<?php

namespace JVG\MandrillBundle\Entity\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JVG\MAndrillBundle\Entity\Message;

class MessageEntityManager
{
    /**
     * @var string
     */
    private $messageEntityCacheNamespace;

    /**
     * @var string
     */
    private $messageEntityProxyNameTemplate;

    /**
     * Constructor
     *
     * @param string $messageEntityCacheNamespace
     * @param string $messageEntityProxyNameTemplate
     */
    public function __construct($messageEntityCacheNamespace, $messageEntityProxyNameTemplate)
    {
        $this->messageEntityCacheNamespace = $messageEntityCacheNamespace;
        $this->messageEntityProxyNameTemplate = $messageEntityProxyNameTemplate;
    }

    /**
     * Create MessageEntity object. Actually a proxy class is created
     * @param array
     * @return MessageEntity
     */
    public function newMessageEntity()
    {
        $messageEntityClass = $this->getMessageEntityProxyClass();

        return new $messageEntityClass();
    }

    /**
     * Get a repository for Message entity
     *
     * @param EntityManager $em
     * @return EntityRepository
     */
    public function getMessageRepository(EntityManager $em)
    {
        return $em->getRepository($this->getMessageEntityProxyClass());
    }

    /**
     * Get full class name of a proxy of Message entity
     *
     * @return string
     */
    public function getMessageEntityProxyClass()
    {
        return sprintf('%s\%s', $this->messageEntityCacheNamespace, sprintf($this->messageEntityProxyNameTemplate, 'MessageEntity'));
    }
}
