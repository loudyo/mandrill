<?php

namespace JVG\MandrillBundle\Entity\Provider;

use JVG\MandrillBundle\Entity\Message;
use JVG\MandrillBundle\Entity\MessageEntity;
use Doctrine\ORM\EntityManager;

/**
 * Message entity provider chain
 */
class MessageEntityProvider
{
    /**
     * @var MessageEntityProviderStorage
     */
    private $messageEntityProviderStorage;

    /**
     * Constructor
     *
     * @param MessageEntityProviderStorage $messageEntityProviderStorage
     */
    public function __construct(MessageEntityProviderStorage $messageEntityProviderStorage)
    {
        $this->messageEntityProviderStorage = $messageEntityProviderStorage;
    }

    /**
     * Find an entity object for given message
     *
     * @param \Doctrine\ORM\EntityManager $em
     * @param Message $message
     * @return MessageEntity
     */
    public function findMessageEntity(EntityManager $em, Message $message)
    {
        $messageEntity = null;
        foreach ($this->messageEntityProviderStorage->getProviders() as $provider) {
            $messageEntity = $provider->findMessageEntity($em, $message);
            if ($messageEntity !== null) {
                break;
            }
        }

        return $messageEntity;
    }
}
