<?php

namespace JVG\MandrillBundle\Entity\Provider;

/**
 * A storage of message entity providers
 */
class MessageEntityProviderStorage
{
    /**
     * @var MessageEntityProviderInterface[]
     */
    private $messageEntityProviders = array();

    /**
     * Add message entity provider
     *
     * @param MessageEntityProviderInterface $provider
     */
    public function addProvider(MessageEntityProviderInterface $provider)
    {
        $this->messageEntityProviders[] = $provider;
    }

    /**
     * Get all message entity providers
     *
     * @return MessageEntityProviderInterface[]
     */
    public function getProviders()
    {
        return $this->messageEntityProviders;
    }

    /**
     * Gets field name for message entity for the given provider
     *
     * @param MessageEntityProviderInterface $provider
     * @return string
     * @throws \RuntimeException
     */
    public function getMessageEntityFieldName(MessageEntityProviderInterface $provider)
    {
        $key = 0;
        for ($i = 0, $size = count($this->messageEntityProviders); $i < $size; $i++) {
            if ($this->messageEntityProviders[$i] === $provider) {
                $key = $i + 1;
                break;
            }
        }

        if ($key === 0) {
            throw new \RuntimeException(
                'The provider for "%s" must be registers in MessageEntityProviderStorage',
                $provider->getMessageEntityClass()
            );
        }

        return sprintf('entity%d', $key);
    }

    /**
     * Gets column name for message entity for the given provider
     *
     * @param MessageEntityProviderInterface $provider
     * @return string
     */
    public function getMessageEntityColumnName(MessageEntityProviderInterface $provider)
    {
        $messageEntityClass = $provider->getMessageEntityClass();
        $prefix = strtolower(substr($messageEntityClass, 0, strpos($messageEntityClass, '\\')));
        if ($prefix === 'oro' || $prefix === 'orocrm') {
            // do not use prefix if entity is a part of BAP and CRM
            $prefix = '';
        } else {
            $prefix .= '_';
        }
        $suffix = strtolower(substr($messageEntityClass, strrpos($messageEntityClass, '\\') + 1));

        return sprintf('entity_%s%s_id', $prefix, $suffix);
    }
}
