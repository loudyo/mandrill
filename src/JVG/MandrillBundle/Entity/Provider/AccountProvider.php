<?php

namespace JVG\MandrillBundle\Entity\Provider;

use JVG\MandrillBundle\Entity\Message;
use Doctrine\ORM\EntityManager;
use Oro\Bundle\UserBundle\Entity\User;
use Oro\Bundle\UserBundle\Entity\Email;

class AccountProvider implements MessageEntityProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getMessageEntityClass()
    {
        return 'OroCRM\Bundle\AccountBundle\Entity\Account';
    }

    /**
     * {@inheritdoc}
     */
    public function findMessageEntity(EntityManager $em, Message $message)
    {
        /** @var User $user */
        $entity = $em->getRepository('OroCRMAccountBundle:Account')
            ->find($message->getEntity());

        return $entity;
    }
}
