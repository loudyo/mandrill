<?php

namespace JVG\MandrillBundle\Entity\Provider;

use JVG\MandrillBundle\Entity\Message;
use Doctrine\ORM\EntityManager;

/**
 * Defines an interface of an message entity provider
 */
interface MessageEntityProviderInterface
{
    /**
     * Get full name of message entity class
     *
     * @return string
     */
    public function getMessageEntityClass();

    /**
     * Find an entity object for given message
     *
     * @param \Doctrine\ORM\EntityManager $em
     * @param Message $message
     * @return mixed
     */
    public function findMessageEntity(EntityManager $em, Message $message);
}
