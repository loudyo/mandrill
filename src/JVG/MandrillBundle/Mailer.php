<?php

namespace JVG\MandrillBundle;

use JVG\MandrillBundle\Model\EmailTemplateUtils;
use JVG\MandrillBundle\Model\MailableInterface;
use Oro\Bundle\EmailBundle\Entity\EmailTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Mailer extends \Mandrill
{
    const TEMPLATE = 'JVGMandrillBundle:Mandrill:email.txt.twig';
    const WEBHOOK_ROUTE = 'jvg_mandrill_hook';
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EmailTemplate
     */
    protected $template;

    /**
     * @var String
     */
    protected $senderEmail;

    /**
     * @var String
     */
    protected $senderName;

    /**
     * @var Bool
     */
    protected $async;

    /**
     * @var String
     */
    protected $ip_pool;

    /**
     * @var String
     */
    protected $send_at;

    /**
     * @var String[]
     */
    protected $_tags;

    /**
     * @var array[]
     */
    protected $_to;

    /**
     * @var array[]
     */
    protected $_vars;

    /**
     * @var array[]
     */
    protected $global_vars;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     * @param Bool $async
     * @param String $ip_pool
     */
    public function __construct($container, $async=false, $ip_pool = null)
    {
        $this->_tags = array();
        $this->_vars = array();
        $this->global_vars = array();
        $this->_to = array();
        $this->async = $async;
        $this->ip_pool = $ip_pool;
        $this->container = $container;
        $config = $container->get('oro_config.global');
        $this->senderEmail = $config->get('oro_notification.email_notification_sender_email');
        $this->senderName  = $config->get('oro_notification.email_notification_sender_name');

        if (!$apiKey = $config->get('jvg_mandrill.api_key')) $apiKey = 'PLEASE_SET_API_KEY';
        parent::__construct($apiKey);
    }

    public function addEntity(MailableInterface $entity)
    {
        $this->_to[] = $entity->getTo();
        $this->_vars[] = $entity->getVars();
    }

    public function setTemplate($id)
    {
        $template = $this->container->get('doctrine.orm.entity_manager')
                         ->getRepository('OroEmailBundle:EmailTemplate')->find($id);
        try {
            $this->templates->info($template->getName());
            $this->template = $template->getName();
        } catch (\Mandrill_Error $e) {
            if($e instanceof \Mandrill_Unknown_Template || $e instanceof \Mandrill_Invalid_Template)
                $this->template = $template;
            else
                throw $e;
        }
    }

    public function setGlobalVars($vars) {
        foreach($vars as $name => $value) {
            $this->global_vars[] = array('name' => $name, 'content' => $value);
        }
    }

    public function setTags($tags)
    {
        foreach ($tags as $tag) {
            $this->_tags[] = $tag->getName();
        }
    }

    public function setSendAt(\DateTime $dateTime){
        $this->send_at = $dateTime->format('Y-m-d H:i:s');
    }

    public function getMessage()
    {   /*
         * @param struct $message the information on the message to send
         *     - html string the full HTML content to be sent
         *     - text string optional full text content to be sent
         *     - subject string the message subject
         *     - from_email string the sender email address.
         *     - to array an array of recipient information.
         *         - to[] struct a single recipient's information.
         *             - email string the email address of the recipient
         *             - name string the optional display name to use for the recipient
         *             - type string the header type to use for the recipient, defaults to "to" if not provided
         */
        $message = array('from_email' => $this->senderEmail,
            'from_name' => $this->senderName,
            'to' => $this->_to,
            'tags' => $this->_tags,
            'global_merge_vars' => $this->global_vars,
            'merge_vars' => $this->_vars
        );
        return $message;
    }

    public function send(){
        $message = $this->getMessage();
        $result = array();
        if (is_string($this->template))  {
            $result = $this->sendTemplate($message);
        } else if (is_object($this->template)) {
            $message['subject'] = EmailTemplateUtils::getCode($this->template->getSubject());
            $message['html'] = EmailTemplateUtils::getCode($this->template->getContent());
            $result = $this->sendMessage($message);
        }
        return $result;
    }

    public function sendMessage(array $message)
    {
        return $this->messages->send(
            $message, $this->async, $this->ip_pool, $this->send_at);
    }

    public function sendTemplate(array $message)
    {

        return $this->messages->sendTemplate($this->template, array(), $message,
             $this->async, $this->ip_pool, $this->send_at
        );
    }

    public function updateWebHookKey(){
        $router = $this->container->get('router');
        $url = $router->generate(self::WEBHOOK_ROUTE, array(), UrlGeneratorInterface::ABSOLUTE_URL);
        $events = array('send', 'hard_bounce', 'soft_bounce', 'spam', 'reject', 'blacklist');
        $desc = 'Messages status update';
        foreach ($this->webhooks->getList() as $item) {
            if ($item['url'] == $url) {
                $id = $item['id'];
            }
        }
        try {
            if (isset($id)) {
                $this->webhooks->delete($id);
            }
            $result = $this->webhooks->add($url, $desc, $events);
        } catch (\Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            $this->container->get('monolog.logger.batch')->addError('A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage());
            // A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
            throw $e;
        }
        return $result['auth_key'];
    }
}
