<?php
namespace JVG\MandrillBundle\Swift\Plugins;

class MandrillPlugin implements \Swift_Events_SendListener
{
    /**
     * The sender to impersonate.
     *
     * @var String
     */
    private $_sender;

    /**
     * Create a new ImpersonatePlugin to impersonate $sender.
     *
     * @param string $sender address
     */
    public function __construct($sender)
    {
        $this->_sender = $sender;
    }

    /**
     * Invoked immediately before the Message is sent.
     *
     * @param \Swift_Events_SendEvent $evt
     */
    public function beforeSendPerformed(\Swift_Events_SendEvent $evt)
    {
        $message = $evt->getMessage();
        $headers = $message->getHeaders();

        $headers->addPathHeader('X-MC-Template', 'Test');

    }

    /**
     * Invoked immediately after the Message is sent.
     *
     * @param \Swift_Events_SendEvent $evt
     */
    public function sendPerformed(\Swift_Events_SendEvent $evt)
    {
        $message = $evt->getMessage();

        // restore original headers
        $headers = $message->getHeaders();

        if ($headers->has('X-Swift-Return-Path')) {
                $message->setReturnPath($headers->get('X-Swift-Return-Path')->getAddress());
                $headers->removeAll('X-Swift-Return-Path');
        }
    }
}
