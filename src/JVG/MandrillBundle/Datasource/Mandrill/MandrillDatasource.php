<?php

namespace JVG\MandrillBundle\Datasource\Mandrill;

use JVG\MandrillBundle\Mailer;

use Oro\Bundle\DataGridBundle\Datagrid\DatagridInterface;
use Oro\Bundle\DataGridBundle\Datasource\DatasourceInterface;
use Oro\Bundle\DataGridBundle\Datasource\ResultRecord;
use Oro\Bundle\DataGridBundle\Datasource\ResultRecordInterface;

class MandrillDatasource implements DatasourceInterface
{
    const TYPE = 'mandrill';

    /**
     * @var Mailer
     */
    protected $mailer;

    /** @var DatagridInterface */
    protected $datagrid;

    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    /**
     * {@inheritDoc}
     */
    public function process(DatagridInterface $grid, array $config)
    {
        $this->datagrid = $grid;

        $grid->setDatasource(clone $this);
    }

    /**
     * @return ResultRecordInterface[]
     */
    public function getResults()
    {

        $results = $this->mailer->templates->getList();
        $rows    = [];
        foreach ($results as $result) {
            $rows[] = new ResultRecord($result);
        }

        return $rows;
    }
}
