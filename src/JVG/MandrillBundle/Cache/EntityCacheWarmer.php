<?php

namespace JVG\MandrillBundle\Cache;

use JVG\MandrillBundle\Entity\Provider\MessageEntityProviderStorage;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmer;
use Symfony\Component\Filesystem\Filesystem;

class EntityCacheWarmer extends CacheWarmer
{
    /**
     * @var MessageEntityProviderStorage
     */
    protected $messageEntityProviderStorage;

    /**
     * @var string
     */
    private $entityCacheDir;

    /**
     * @var string
     */
    private $entityCacheNamespace;

    /**
     * @var string
     */
    private $entityProxyNameTemplate;

    /**
     * Constructor.
     *
     * @param MessageEntityProviderStorage $messageEntityProviderStorage
     * @param string                    $entityCacheDir
     * @param string                    $entityCacheNamespace
     * @param string                    $entityProxyNameTemplate
     */
    public function __construct(
        MessageEntityProviderStorage $messageEntityProviderStorage,
        $entityCacheDir,
        $entityCacheNamespace,
        $entityProxyNameTemplate
    ) {
        $this->messageEntityProviderStorage = $messageEntityProviderStorage;
        $this->entityCacheDir            = $entityCacheDir;
        $this->entityCacheNamespace      = $entityCacheNamespace;
        $this->entityProxyNameTemplate   = $entityProxyNameTemplate;
    }

    /**
     * {inheritdoc}
     */
    public function warmUp($cacheDir)
    {
        $fs   = $this->createFilesystem();
        $twig = $this->createTwigEnvironment();

        $this->processEmailAddressTemplate($fs, $twig);
    }

    /**
     * {inheritdoc}
     */
    public function isOptional()
    {
        return false;
    }

    /**
     * Create Filesystem object
     *
     * @return Filesystem
     */
    protected function createFilesystem()
    {
        return new Filesystem();
    }

    /**
     * Create Twig_Environment object
     *
     * @return \Twig_Environment
     */
    protected function createTwigEnvironment()
    {
        $entityTemplateDir = __DIR__ . '/../Resources/cache/MessageEntity';

        return new \Twig_Environment(new \Twig_Loader_Filesystem($entityTemplateDir));
    }

    /**
     * Create a proxy class for EmailAddress entity and save it in cache
     *
     * @param Filesystem        $fs
     * @param \Twig_Environment $twig
     */
    protected function processEmailAddressTemplate(Filesystem $fs, \Twig_Environment $twig)
    {
        // Ensure the cache directory exists
        if (!$fs->exists($this->entityCacheDir)) {
            $fs->mkdir($this->entityCacheDir, 0777);
        }

        $args      = array();
        $providers = $this->messageEntityProviderStorage->getProviders();
        foreach ($providers as $provider) {
            $args[] = array(
                'targetEntity' => $provider->getMessageEntityClass(),
                'columnName'   => $this->messageEntityProviderStorage->getMessageEntityColumnName($provider),
                'fieldName'    => $this->messageEntityProviderStorage->getMessageEntityFieldName($provider)
            );
        }

        $className  = sprintf($this->entityProxyNameTemplate, 'MessageEntity');
        $twigParams = array(
            'namespace' => $this->entityCacheNamespace,
            'className' => $className,
            'entities'  => $args
        );

        // generate a proxy class
        $content = $twig->render('MessageEntity.php.twig', $twigParams);
        $this->writeCacheFile(
            sprintf('%s%s%s.php', $this->entityCacheDir, DIRECTORY_SEPARATOR, $className),
            $content
        );
        // generate ORM mappings for a proxy class
        $content = $twig->render('MessageEntity.orm.yml.twig', $twigParams);
        $this->writeCacheFile(
            sprintf('%s%s%s.orm.yml', $this->entityCacheDir, DIRECTORY_SEPARATOR, $className),
            $content
        );
    }
}
