<?php

namespace JVG\MandrillBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class MessageEntityConfigurationPass implements CompilerPassInterface
{
    const SERVICE_KEY = 'jvg_mandrill.message_entity.provider.storage';
    const TAG = 'jvg_mandrill.message_entity.provider';

    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(self::SERVICE_KEY)) {
            return;
        }
        $storageDefinition = $container->getDefinition(self::SERVICE_KEY);

        $providers = $this->loadProviders($container);
        foreach ($providers as $providerServiceId) {
            $storageDefinition->addMethodCall('addProvider', array(new Reference($providerServiceId)));
        }

        $this->setMessageEntityResolver($container);
    }

    /**
     * Load services implements an email owner providers
     *
     * @param ContainerBuilder $container
     * @return array
     */
    protected function loadProviders(ContainerBuilder $container)
    {
        $taggedServices = $container->findTaggedServiceIds(self::TAG);
        $providers = array();
        foreach ($taggedServices as $id => $tagAttributes) {
            $order = PHP_INT_MAX;
            foreach ($tagAttributes as $attributes) {
                if (!empty($attributes['order'])) {
                    $order = (int)$attributes['order'];
                    break;
                }
            }
            $providers[$order] = $id;
        }
        ksort($providers);

        return $providers;
    }

    /**
     * Register a proxy of EmailAddress entity in doctrine ORM
     *
     * @param ContainerBuilder $container
     */
    protected function setMessageEntityResolver(ContainerBuilder $container)
    {
        if ($container->hasDefinition('doctrine.orm.listeners.resolve_target_entity')) {
            $def = $container->getDefinition('doctrine.orm.listeners.resolve_target_entity');
            $def->addMethodCall(
                'addResolveTargetEntity',
                array(
                    'JVG\MandrillBundle\Entity\MessageEntity',
                    sprintf('%s\MessageEntityProxy', $container->getParameter('jvg_mandrill.message_entity.cache_namespace')),
                    array()
                )
            );
        }
    }
}
