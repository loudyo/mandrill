<?php

namespace JVG\MandrillBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class DoctrineFunctionLoaderPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        $configuration = $container->findDefinition('doctrine.orm.configuration');
        $configuration->addMethodCall('addCustomStringFunction', array('GROUP_CONCAT','JVG\MandrillBundle\DQL\String\GroupConcat'));
    }
}
