<?php

namespace JVG\MandrillBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

class SecurityLoaderPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        $requestMatcher = $this->createRequestMatcher($container, '^/mandrill/hook$');
        $this->createAuthentication($container, $requestMatcher);
        $this->createAuthorization($container, $requestMatcher);
    }

    private function createAuthentication(ContainerBuilder $container, Reference $matcher)
    {
        // Register listeners
        $listeners = array();

        // Channel listener
        $listeners[] = new Reference('security.channel_listener');

        // Authentication listeners
        $listenerId = 'security.authentication.listener.anonymous.mandrill';
        $container->setDefinition($listenerId, new DefinitionDecorator('security.authentication.listener.anonymous'));
        $listeners[] = new Reference($listenerId);

        // Access listener
        $listeners[] = new Reference('security.access_listener');

        // Exception listener
        $exceptionListenerId = 'security.exception_listener.mandrill';
        $container->setDefinition($exceptionListenerId, new DefinitionDecorator('security.exception_listener'));
        $exceptionListener = new Reference($exceptionListenerId);

        $contextId = 'security.firewall.map.context.mandrill';
        $context = $container->setDefinition($contextId, new DefinitionDecorator('security.firewall.context'));
        $context->replaceArgument(0, $listeners)->replaceArgument(1, $exceptionListener);

        // modify firewall map
        $firewallMap = $container->findDefinition('security.firewall.map');
        $map = $firewallMap->getArgument(1);
        $offset = sizeof($map)-1;
        $newMap = array_slice($map, 0, $offset, true) +
        array($contextId => $matcher) +
        array_slice($map, $offset, NULL, true);
        $firewallMap->replaceArgument(1, $newMap);
    }
    private function createAuthorization(ContainerBuilder $container, Reference $matcher)
    {
        $accessMap = $container->findDefinition('security.access_map');
        $accessMap->addMethodCall('add', array($matcher , array('IS_AUTHENTICATED_ANONYMOUSLY')));
    }

    private function createRequestMatcher($container, $path = null, $host = null, $methods = array(), $ip = null, array $attributes = array())
    {
        $serialized = serialize(array($path, $host, $methods, $ip, $attributes));
        $id = 'security.request_matcher.'.md5($serialized).sha1($serialized);

        if ($methods) {
            $methods = array_map('strtoupper', (array) $methods);
        }

        // only add arguments that are necessary
        $arguments = array($path, $host, $methods, $ip, $attributes);
        while (count($arguments) > 0 && !end($arguments)) {
            array_pop($arguments);
        }

        $container
            ->register($id, '%security.matcher.class%')
            ->setPublic(false)
            ->setArguments($arguments)
        ;

        return new Reference($id);
    }
}
