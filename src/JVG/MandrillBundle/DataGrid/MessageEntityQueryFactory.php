<?php

namespace JVG\MandrillBundle\Datagrid;

use JVG\MandrillBundle\Entity\Provider\MessageEntityProviderStorage;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class MessageEntityQueryFactory
{
    /**
     * A list of field names of all email owners
     *
     * @var string[]
     */
    protected $messageEntityFieldNames = array();

    /**
     * @var string
     */
    protected $messageEntityExpression;

    /**
     * @var RegistryInterface
     */
    protected $registry;

    /**
     * @var string
     */
    protected $className;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @param RegistryInterface $registry
     * @param string $className
     * @param MessageEntityProviderStorage $messageEntityProviderStorage
     */
    public function __construct(
        RegistryInterface $registry,
        $className,
        MessageEntityProviderStorage $messageEntityProviderStorage
    ) {
        $this->registry = $registry;
        $this->className = $className;
        $this->alias = 'e'; // default

        $providers = $messageEntityProviderStorage->getProviders();
        foreach ($providers as $provider) {
            $this->messageEntityFieldNames[] = $messageEntityProviderStorage->getMessageEntityFieldName($provider);
        }

        $names = array();
        foreach ($this->messageEntityFieldNames as $fieldName) {
            $names[] = sprintf('%s.name', $fieldName);
        }

        //COALESCE(entity1.name, entity2.name, '')
        $this->messageEntityExpression = sprintf('COALESCE(%s, \'\')', implode(', ', $names));
    }

    /**
     * @return string
     */
    public function getMessageEntityExpression()
    {
        return $this->messageEntityExpression;
    }

    /**
     * @param QueryBuilder $qb
     * @return $this
     */
    public function prepareQuery(QueryBuilder $qb)
    {
        foreach ($this->messageEntityFieldNames as $fieldName) {
            $qb->leftJoin('e.' . $fieldName, $fieldName);
        }
        return $this;
    }

}
