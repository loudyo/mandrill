<?php

namespace JVG\MandrillBundle\DataGrid\Extension\MassAction;

use Oro\Bundle\DataGridBundle\Extension\Action\ActionConfiguration;
use Oro\Bundle\DataGridBundle\Extension\MassAction\Actions\AbstractMassAction;

use Symfony\Component\DependencyInjection\ContainerInterface;


class MandrillMassAction extends AbstractMassAction
{
    /** @var array */
    protected $requiredOptions = ['handler', 'entity_name', 'data_identifier'];

    /** @var array */
    protected $defaultOptions = array(
        'frontend_handle' => 'redirect',
        'handler' => 'jvg_mandrill.mass_action.data_handler',
        'icon' => 'envelope',
        'label' => 'Mass mail',
        'frontend_type' => 'mandrill-mass',
        'route' => 'jvg_mandrill_massaction',
        'route_parameters' => array(),
        'data_identifier' => 'id',
    );

    /**
     * {@inheritDoc}
     */
    public function setOptions(ActionConfiguration $options)
    {
        $this->setDefaultOptions($options);
        return parent::setOptions($options);
    }

    /**
     * @param ActionConfiguration $options
     */
    protected function setDefaultOptions(ActionConfiguration $options)
    {
        foreach ($this->defaultOptions as $name => $value) {
            if (!isset($options[$name])) {
                $options[$name] = $value;
            }
        }
    }
}
