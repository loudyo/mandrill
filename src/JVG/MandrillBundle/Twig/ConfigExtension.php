<?php

namespace JVG\MandrillBundle\Twig;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;

class ConfigExtension extends \Twig_Extension
{
    /**
     * @var ConfigManager
     */
    protected $config;

    /**
     * @param ConfigManager $config
     */
    public function __construct(ConfigManager $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'get_mandrill_api_key' => new \Twig_Function_Method(
                $this,
                'getApiKey'
            ),
        );
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->config->get('jvg_mandrill.api_key') ?
            $this->config->get('jvg_mandrill.api_key') :
            'PLEASE_SET_MANDRILL_API_KEY';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'jvg_mandrill.settings';
    }
}
