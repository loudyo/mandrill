<?php

namespace JVG\MandrillBundle\Controller;

use JVG\MandrillBundle\Entity\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionDispatcher;
use OroCRM\Bundle\AccountBundle\Entity\Account;

/**
 * @Route("/mandrill")
 */
class MandrillController extends Controller
{
    const WEBHOOK_POST_PARAMETER = 'mandrill_events';
    const WEBHOOK_HEADER = 'X-Mandrill-Signature';
    /**
     * @Route("/templates")
     * @Template()
     */
    public function templatesAction()
    {
        return $this->render('JVGMandrillBundle:Mandrill:templates.html.twig');
    }

    /**
     * @Route("/messages")
     * @Template()
     */
    public function messagesAction()
    {
        return $this->render('JVGMandrillBundle:Mandrill:messages.html.twig');
    }

    /**
     * @Route("/message/{message}", name="jvg_mandrill_message", requirements={"id"="^\d+$"})
     * @Template()
     */
    public function messageAction(Message $message)
    {
        $mailer = $this->get('jvg_mandrill.mailer');
        try {
            $result = $mailer->messages->info($message->getMandrillId());
        } catch (\Mandrill_Error $e) {
            throw $e;
        }
        return $this->render('JVGMandrillBundle:Mandrill:message.html.twig', array('entity' => $message));
    }

    /**
     * @Route("/hook", name="jvg_mandrill_hook")
     */
    public function hookAction(Request $request)
    {
        if('HEAD' === $request->getMethod())return new Response(); //ok for mandrill checks
        $webhookKey = $this->get('oro_config.global')->get('jvg_mandrill.webhook_key');
        $signature = $request->headers->get(self::WEBHOOK_HEADER);
        $data = $request->request->get(self::WEBHOOK_POST_PARAMETER);
        $this->get('monolog.logger.request')->addDebug($data);
        $calculated = $this->generateSignature($webhookKey, $request->getUri(), array(self::WEBHOOK_POST_PARAMETER => $data));
        if ($signature === $calculated) {
            // handle data
            $data = json_decode($data);

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('JVGMandrillBundle:Message');

            if (is_object($data)) { //single item
                $message = $repository->findOneBy(array('mandrillId' => $data->_id));
                $message->setStatus($data->msg->state);
                $message->setMandrillId($data->msg->_id);
                $em->persist($message);
            } else if (is_array($data) && sizeof($data)) {
                foreach ($data as $item ) {
                    $message = $repository->findOneBy(array('mandrillId' => $item->_id));
                    if (!$message)
                        continue;
                    $message->setStatus($item->msg->state);
                    $message->setMandrillId($item->msg->_id);
                    $em->persist($message);
                }
                $em->flush();
            }
            return new Response();
        }
        return new Response('Access denied', 401);
    }

    /**
     * Generates a base64-encoded signature for a Mandrill webhook request.
     * @param string $webhook_key the webhook's authentication key
     * @param string $url the webhook url
     * @param array $params the request's POST parameters
     * @return String
     */
    private function generateSignature($webhook_key, $url, $params) {
        $signed_data = $url;
        ksort($params);
        foreach ($params as $key => $value) {
            $signed_data .= $key;
            $signed_data .= $value;
        }
        return base64_encode(hash_hmac('sha1', $signed_data, $webhook_key, true));
    }

    /**
     * @Route("/choose/", name="jvg_mandrill_choose" )
     * @Template()
     */
    public function chooseAction(array $accounts = null)
    {
        try {
            if ($this->get('jvg_mandrill.form.handler.account')->process()) {
                $this->get('session')->getFlashBag()->add(
                    'success', 'Successful'
                );

                return $this->get('oro_ui.router')->redirectAfterSave(
                    [],
                    ['route' => 'orocrm_account_index']
                );
            }
        } catch (\Mandrill_Error $e) {
            if ($e instanceof \Mandrill_Invalid_Key) {
                $this->get('session')->getFlashBag()->add(
                    'error', 'Invalid API Key'
                );
                return $this->redirect($this->generateUrl('oro_config_configuration_system',[
                    'activeGroup'=>'platform',
                    'activeSubGroup' => 'jvg_extensions'
                ]));
            }
        }

        return $this->render('JVGMandrillBundle:Mandrill:choose.html.twig', array(
            'form'    => $this->get('jvg_mandrill.form.account')->setData(array('accounts' => $accounts))->createView()
        ));
    }

    /**
     * @Route(
     *      "/{gridName}/massAction/{actionName}",
     *      name="jvg_mandrill_massaction",
     *      requirements={"gridName"="[\w-]+", "actionName"="[\w-]+"},
     *      options={"expose"=true}
     * )
     */
    public function massActionAction($gridName, $actionName)
    {
        /** @var MassActionDispatcher $massActionDispatcher */
        $massActionDispatcher = $this->get('oro_datagrid.mass_action.dispatcher');

        $accounts = $massActionDispatcher->dispatchByRequest($gridName, $actionName, $this->getRequest());

        return $this->chooseAction($accounts);
    }
}
