<?php

namespace JVG\MandrillBundle\Model;

use Oro\Bundle\EntityConfigBundle\Config\ConfigInterface;
use Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

abstract class MailableAbstract implements MailableInterface{

    const NAME_PATH = 'name';
    const FIRST_NAME_PATH = 'firstname';
    const LAST_NAME_PATH = 'lastname';
    protected $entityClass;
    protected $emailPaths;
    protected $entity;

    /**
     * @var PropertyAccessor
     */
    protected $accessor = null;

    /**
     * @var string
     */
    protected $email;

    /*
     * @var ConfigProvider
     */
    protected $configProvider;

    /**
     * @var ConfigInterface[]
     */
    protected $fields;

    public function setConfigProvider(ConfigProvider $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    public function setClass($entityClass)
    {
        $this->entityClass = $entityClass;
        $this->setAvailableFields();
    }

    public function getClass()
    {
        return $this->entityClass;
    }

    public function setEmailPaths($emailPaths)
    {
        $this->emailPaths = $emailPaths;
    }

    public function setAvailableFields()
    {
        $this->fields = $this->configProvider->filter(
            function (ConfigInterface $fieldConfig) {
                return $fieldConfig->is('available_in_template');
            },
            $this->entityClass
        );
    }

    public function getVars() {
        $vars = array();
        foreach($this->fields as $field) {
            $fieldName = $field->getId()->getFieldName();
            $vars[] = array('name' => $fieldName, 'content' => (string)$this->getPropertyByPath($fieldName));
        }
        return array('rcpt' => $this->getEmail(), 'vars' => $vars);
    }

    /**
     * @return PropertyAccessor
     */
    public function getAccessor()
    {
        if (is_null($this->accessor))
            $this->accessor = PropertyAccess::createPropertyAccessor();
        return $this->accessor;
    }

    /**
     * Get email address
     *
     * @return string|null
     */
    public function getEmail()
    {
        if (is_null($this->email)) {
            foreach ($this->emailPaths as $path){
                if ($this->getPropertyByPath($path)) {
                    $this->email = (string)$this->getPropertyByPath($path);
                    break;
                }
            }
        }
        return $this->email;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {   if (!$name = (string)$this->getPropertyByPath(self::NAME_PATH))
            $name = sprintf('%s %s', $this->getFirstName(), $this->getLastName());
        return $name;
    }

    public function getTo()
    {
        return array('email' => $this->getEmail(), 'name' => $this->getName());
    }

    /**
     * Return first name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->getPropertyByPath(self::FIRST_NAME_PATH);
    }

    /**
     * Return last name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->getPropertyByPath(self::LAST_NAME_PATH);
    }

    public function getPropertyByPath($path)
    {
        try {
            return $this->getAccessor()->getValue($this->entity, $path);
        } catch (\RuntimeException $e) {
            if($e instanceof UnexpectedTypeException OR $e instanceof NoSuchPropertyException)
                return null;
        }
    }

    public function isMailable()
    {
        return is_string($this->getEmail());
    }
}