<?php

namespace JVG\MandrillBundle\Model;
use Oro\Bundle\EmailBundle\Entity\EmailOwnerInterface;

/**
 * Represents an mailable entity
 */
interface MailableInterface
{
    /*
     * @return bool
     */
    public function isMailable();

    /**
     * Get email address
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Get class
     *
     * @return string|null
     */
    public function getClass();

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Get to
     *
     * @return array
     */
    public function getTo();

    /**
     * Get vars
     *
     * @return array
     */
    public function getVars();

    /**
     * Wrap entity
     * @param mixed
     * @return MailableInterface
     */
    public function factory($entity);

    /**
     * Get wrapped entity
     * @return mixed
     */
    public function getEntity();
}
