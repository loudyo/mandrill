<?php

namespace JVG\MandrillBundle\Model;

class MailableEntity extends MailableAbstract
{

    protected $entity;

    /*
     * @var String
     */
    protected $emailPaths;

    /**
     * @var string
     */
    protected $email;

    public function factory($entity) {
        $this->entity = $entity;
        $this->email = null;
        return $this;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getId() {
        return $this->entity->getId();
    }


}