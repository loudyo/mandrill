<?php
/**
 * Created by PhpStorm.
 * User: lou
 * Date: 7/17/14
 * Time: 3:26 PM
 */

namespace JVG\MandrillBundle\Model;


class EmailTemplateUtils {

    /**
     * {{ entity.someVar }} => *|SOMEVAR|*
     *
     * @param String $text
     * @return String
     */
    public static function getCode($text){
        return preg_replace_callback('{{{ entity\.(.*) }}}', function ($matches) {
            return '*|' . strtoupper($matches[1]) . '|*';
        }, $text);
    }
}